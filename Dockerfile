FROM maven:3.8.4-openjdk-21-slim AS maven

COPY ./src /home/app/src
COPY ./pom.xml /home/app
RUN mvn -f /home/app/pom.xml package -DskipTests

FROM openjdk:21-jdk-slim
ENV DB_ADDRESS db_address
ENV DB_USER db_user
ENV DB_PASSWORD db_password
COPY --from=maven /home/app/target/demo-0.0.1-SNAPSHOT.jar demo.jar

ENTRYPOINT ["java", "-jar", "/demo.jar"]




