package com.example.demo.Controllers;

import com.example.demo.Models.Player;
import com.example.demo.Services.PlayerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(path="/api/v1/players")
public class PlayerController {

    private final PlayerService playerService;

    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @GetMapping
    public List<Player> getPlayers() {
        return playerService.getPlayer();
    }

    @GetMapping("/{id}")
    public Player getPlayerById(@PathVariable Long id) {
        return playerService.getPlayerById(id);
    }

    @GetMapping("/ordered")
    public List<Player> findByOrderByIdAsc() {
        return playerService.findByOrderByIdAsc();
    }

    @PostMapping()
    public void createPlayer(@RequestBody Player player) {
        playerService.createPlayer(player);
    }

    @PutMapping(path = "{id}")
    public void updatePlayer(@PathVariable Long id, @RequestBody Player player) {
         playerService.updatePlayer(id, player);
    }

    @DeleteMapping(path = "{id}")
    public void deletePlayer(@PathVariable Long id, @RequestBody Player player) {
        playerService.deletePlayer(id, player);
    }
}
