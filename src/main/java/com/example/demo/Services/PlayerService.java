package com.example.demo.Services;

import com.example.demo.Models.Player;
import com.example.demo.Repositories.PlayerRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerService implements IPlayerService {

    private final PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    //Return a list of players
    @Override
    public List<Player> getPlayer() {
        return playerRepository.findAll();
    }

    //Return a player based on id
    @Override
    public Player getPlayerById(Long id) {
        try {
            Optional<Player> playerOptional = playerRepository.findById(id);
            return playerOptional.orElseThrow(() -> new EntityNotFoundException("Player doesn't exist"));
        }
        catch(Exception e) {
            throw new RuntimeException("Internal server error.");
        }
    }

    @Override
    public List<Player> findByOrderByIdAsc() {
        return playerRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    //Create a Player
    @Override
    public void createPlayer(Player player) {
        try {
            String lastName = player.getLastName().toLowerCase();
            Optional<Player> playerOptional = playerRepository.getByLastName(lastName);
            if (playerOptional.isPresent()) {
                throw new IllegalStateException(String.format("Player %s already exist.", player.getLastName()));
            }
            player.setLastName(lastName);
            playerRepository.save(player);
        }
        catch (Exception e) {
            throw new RuntimeException("Internal server error");
        }
    }

    //Updating the Player by ID
    @Override
    public void updatePlayer(Long id, Player player) {
        try {
            Optional<Player> playerOptional = playerRepository.findById(id);

            if (playerOptional.isPresent()) {
                Player playerToUpdate = playerOptional.get();

                playerToUpdate.setFirstName(player.getFirstName());
                playerToUpdate.setLastName(player.getLastName());
                playerToUpdate.setTeam(player.getTeam());
                playerToUpdate.setNationality(player.getNationality());
                playerToUpdate.setAge(player.getAge());
                playerToUpdate.setShirtNumber(player.getShirtNumber());

                playerRepository.save(playerToUpdate);
            }
            else {
                throw new IllegalStateException(String.format("Player with id %s already exists.", id));
            }
        }
        catch (Exception e) {
            throw new RuntimeException("Internal Server Error.");
        }
    }

    //Delete the player by ID
    @Override
    public void deletePlayer(Long id, Player player) {
        try {
            Optional<Player> playerOptional = playerRepository.findById(id);

            if (playerOptional.isPresent()) {
                Player playerToDelete = playerOptional.get();

                playerRepository.delete((playerToDelete));
            }
            else {
                throw new IllegalStateException("Player with id %s doesn't exist");
            }
        }
        catch (IllegalStateException e) {
            throw new RuntimeException("Internal Server Error.");
        }
    }
}
