package com.example.demo.Services;

import com.example.demo.Models.Player;

import java.util.List;

public interface IPlayerService {
    List<Player> getPlayer();
    Player getPlayerById(Long id);
    List<Player> findByOrderByIdAsc();
    void createPlayer(Player player);
    void updatePlayer(Long id, Player player);
    void deletePlayer(Long id, Player player);
}
