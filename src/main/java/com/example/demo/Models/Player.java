package com.example.demo.Models;

import jakarta.persistence.*;

@Entity
@Table(name="players")
public class Player {
    @SequenceGenerator(
            name = "player_sequence",
            allocationSize = 1
    )

    @GeneratedValue(
            generator = "player_sequence",
            strategy = GenerationType.SEQUENCE
    )

    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private String team;
    private String nationality;
    private int age;
    private int shirtNumber;

    public Player() {

    }

    public Player(String firstName, String lastName, String team, String nationality, int age, int shirtNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
        this.nationality = nationality;
        this.age = age;
        this.shirtNumber = shirtNumber;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getShirtNumber() {
        return shirtNumber;
    }

    public void setShirtNumber(int shirtNumber) {
        this.shirtNumber = shirtNumber;
    }
}



