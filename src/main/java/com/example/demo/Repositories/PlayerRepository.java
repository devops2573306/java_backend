package com.example.demo.Repositories;

import com.example.demo.Models.Player;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PlayerRepository extends JpaRepository<Player, Long> {
    Optional<Player> findById(Long id);
    Optional<Player> getByLastName(String lastName);
    List<Player> findByOrderByIdAsc();
}
